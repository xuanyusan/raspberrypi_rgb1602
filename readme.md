# Raspberrypi_RGB1602

## 介绍
Raspberrypi_RGB1602是使用RGB1602模块的简单例子。（不一定适用于其他同名模块，需要分析接口关系）

## 软件架构
```sh
|- python
    |-config.py              # 配置文件
    |-MCP23017.py            # MCP输入文件，提供给LCD1602模块输入命令和输入数据等接口函数
    |-LCD1602_time.py        # 打印时间的主入口文件
    |-LCD1602_ip.py          # 打印IP地址的主入口文件
    |-LCD1602_weather.py     # 打印天气的主入口文件
|- c
    |-...
```

## 使用教程
使用过程中遇到的问题可以参见[这篇博客]()