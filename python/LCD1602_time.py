from LCD1602 import print_lcd, init_lcd
import time

def print_time():
    d = time.strftime("%Y-%m-%d", time.localtime())
    t = time.strftime("%H:%M:%S", time.localtime())
    print_lcd(0, 0, d)
    print_lcd(0, 1, t)
    
if __name__ == "__main__":
    init_lcd()
    while True:
        print_time()
        time.sleep(0.1)