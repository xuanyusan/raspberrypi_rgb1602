from LCD1602 import print_lcd, init_lcd
from char import injectRAMContent, DefaultRAMContent

if __name__ == "__main__":
    init_lcd()
    injectRAMContent(DefaultRAMContent)
    print_lcd(0, 0, chr(0x00)+chr(0x01)+chr(0x02)+chr(0x03)+chr(0x04)+chr(0x05)+chr(0x06)+chr(0x07))
    print_lcd(0, 1, "アリガトウ")