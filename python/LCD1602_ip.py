from LCD1602 import print_lcd, init_lcd
import time
import socket

def get_host_ip():
    ip = ""
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
    return ip

hostname = socket.gethostname()
ip = get_host_ip()

def print_ip():
    print_lcd(0, 0, hostname)
    print_lcd(0, 1, ip)
    
if __name__ == "__main__":
    init_lcd()
    print_ip()